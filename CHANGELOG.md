# Change Log

## 0.5.2.4

- Fix: Health change validation would fail if the numbers were `null` (as one would sometimes get from clearing a field) [#19]

## 0.5.2.3

- Fix: Fast Heal example buffs had incorrect descriptions. [!2] (thanks to @VorpalThunder)

## 0.5.2.2

- Fix: stabilized of null error

## 0.5.2.1

- Fix: i18n strings were not correctly replaced.

## 0.5.2

- Fix: Chat cards in popped out chat log were rendered wrong.
- Fix: Dying flagged items were not disabled on successful stabilization.
- i18n: Improved translation support.
- Change: Most settings are directly in Foundry menu. Old settings are migrated by GM.
- Fix: Auto-bleed did not bleed with dying condition.
- Change: Fairly major refactoring. Things may have broken.

## 0.5.1

- Foundry v10 compatibility, v9 and older support dropped

## 0.5.0

- Fix permission fetching
- Smaller releases

## 0.4.2

- Internal: Bundling via esbuild.
- Internal: Less swapped for SCSS.

## 0.4.1

- New: Formulas are provided relevant roll data.
- Fix: Items were iterated over twice for bleed.
- Fix: Dying condition bleed was handled poorly.

## 0.4.0

- New: Easy configuration of item-based bleeding and regeneration.
- New: Item Hints custom handler.
- New: Regen boolean flag alone now uses item level.
- New: Fast healing is now possible.
- New: Example buff selection expanded.
- New: Any item capable of being active can include the relevant info.

### Breaking Changes

- Changed: `bleeding` flag is now `bleed`

## 0.3.2.6

- Fix: Error each round if no stabilization occurs.

## 0.3.2.5

- Fix: Bleed buff that was based on the item level was not working as intended [baeebb88]

## 0.3.2.3 – 0.3.2.4

- Fix: PF1 0.80.14 dictionary/boolean flag compatibility.

## 0.3.2.2

- Fix: Ignored regen values caused regeneration to be calculated as -Infinity.

## 0.3.2

- New: Formula-based bleed via dictionary flags. (!1 by @claudekennilol)
- New: 2d6 bleed buff as example of the dictionary flag based bleed.

## 0.3.1

- Fixed: Fast healing applied to both nonlethal damage and lost health when it should only apply to one.
- Internal restructuring.
- Foundry 0.8 support removed.

## 0.3.0.4

- Fixed: Negative regeneration/fasthealing was treated as positive.

## 0.3.0.3

- Fixed: Bad permission checking.

## 0.3.0.2

- Fixed: Weird behaviour caused by players sometimes handling actors they have no permissions to.

## 0.3.0.1

- Fixed: Regeneration did not work for anything but nonlethal damage.

## 0.3.0

- Changed: Bleeding condition is no longer treated as dying condition.
- Changed: Dying is handled via buff with `dying` boolean flag.
- Changed: Bleeding buff no longer requires the bleeding condition to be enabled on top.
- New: Health Conditions compendium pack with Dying & Bleeding example buffs.

## 0.2.0.1

- Fixed: Defeated combatants were not ignored correctly.
- Fixed: Disabled bleeding buffs were still taken into account.

## 0.2.0

- Fix: Regeneration no longer incorrectly disables bleed.
- New: Bleeding damage via bleed buff (requires `bleeding` boolean flag set, amount configured via buff level).
- New: Stabilization check option.
- Removed: Foundry 0.7.x support in anticipation of 0.9
- Removed: Various settings.

## 0.1.7.1

- Fixed: User retrieval for whisper targets was attempting to get actors instead.

## 0.1.7

- Foundry 0.7.x cross-compatibility
- Changed: Chat card generation and compacting.

## 0.1.6

- Regen state update merged into actor health update, may have significant performance benefit.
- Defeated combatants bail out of the logic earlier to avoid unnecessary processing. Insignificant performance boost.
- Foundry 0.8 compatibility.

## 0.1.5

- Fixed: Healing triggering multiple times on same round (changing initiative, combatants being added or removed, or cycling turn back and forth).
- Fixed: Healing triggering before combat has started.

## 0.1.4

- New: Configuration dialog.
- New: Translation files.

## 0.1.3

- Stopgap fix for bad behaviour. Actual cause unknown.

## 0.1.2

- Hotfix. You saw nothing.

## 0.1.1

### New

- Bleeding out now marks defeated.
- Full transparency option to not hide chat cards.
- Messages are now compacted.

### Fixed

- Handling is now done on GM side solely, fixing variety of horrifying issues.
- Dead characters don't continue to bleed. There's only so much blood.
