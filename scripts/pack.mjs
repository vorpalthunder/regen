import nedb from 'nedb';
import fs from 'fs';
import path from 'path';
import process from 'node:process';

const packSourceBase = './packs/';
const packDest = './dist/packs/';

const args = process.argv.slice(2);
const focus = [];
let doPacking = false, doUnpacking = false, doCompact = false, doEmpty = false;
while (args.length) {
	const arg = args.shift();
	switch (arg) {
		case '--pack': doPacking = true; break;
		case '--unpack': doUnpacking = true; break;
		case '--compact': doCompact = true; break;
		case '--empty': doEmpty = true; break;
		default: focus.push(arg);
	}
}

// console.log(focus);

const slugify = (name) => {
	return name.trim()
		.replace(/[\s|.?\\/:*"><]+/g, '-'); // OS unsafe characters
}

async function packCompendiums() {
	const packs = [];

	fs.readdirSync(packSourceBase, { withFileTypes: true }).forEach((d) => {
		if (d.isDirectory()) packs.push({ name: d.name, path: path.join(packSourceBase, d.name) });
	});

	console.log(' ? Found', packs.length, 'packs\n');
	let packsProcessed = 0;

	const waitingPacks = [];

	const handlePack = async (pack) => {
		const packPath = path.join(packDest, `${pack.name}.db`);
		console.log('Generating:', path.parse(packPath).base);

		const knownIds = new Set();

		const files = [];
		fs.readdirSync(pack.path, { withFileTypes: true })
			.filter((e) => e.isFile)
			.map((e) => files.push({ name: e.name, path: path.join(pack.path, e.name) }));

		console.log('Found', files.length, 'entries');
		if (files.length === 0) return void console.log('');

		const db = new nedb({ filename: packPath, autoload: true });
		db.persistence.stopAutocompaction();

		const p = new Promise(resolve => db.on('compaction.done', () => resolve()));
		waitingPacks.push(p);

		const handleFile = async (file) => {
			console.log(' +', file.name);
			const json = JSON.parse(fs.readFileSync(file.path, { encoding: 'utf-8' }));
			if (knownIds.has(json._id)) console.warn('--- Duplicate ID:', json._id, 'in', json.name);
			knownIds.add(json._id);
			if (json) {
				const old = await new Promise(resolve => {
					db.findOne({ _id: json._id }, (err, doc) => {
						if (err) console.error(err.key, err.errorType);
						resolve(doc);
					});
				});

				if (old) {
					// console.log('<<< Deleting:', json._id);
					// Delete to avoid old data cruft
					await new Promise(resolve => {
						db.remove({ _id: json._id }, (err) => resolve(!err));
					});
				}

				// console.log('+++ Inserting:', json._id);
				await new Promise(resolve => {
					db.insert(json, (err) => {
						if (err) console.log('--- error:', err.key, err.errorType);
						resolve(!err);
					});
				});
			}
		}

		const waitingFiles = [];
		for (const file of files)
			waitingFiles.push(handleFile(file));

		await Promise.all(waitingFiles);

		console.log(''); // Empty line

		db.persistence.compactDatafile();
		packsProcessed += 1;
	}

	// Process files
	for (const pack of packs) {
		if (focus.length && !focus.includes(pack.name)) return;
		await handlePack(pack);
	}

	console.log('Waiting compaction...');

	await Promise.allSettled(waitingPacks);
	console.log('Compaction done!');
}

async function unpackCompendiums() {
	const packs = [];

	console.log('Reading packed data');
	fs.readdirSync(packDest, { withFileTypes: true })
		.forEach((d) => {
			if (!d.isDirectory())
				packs.push({ name: d.name, path: path.join(packDest, d.name) });
		});

	console.log('Creating destination folder');
	fs.mkdirSync(packSourceBase, { recursive: true });

	const unpackPack = async (pack) => {
		const packId = path.parse(pack.name).name;
		if (focus.length && !focus.includes(pack.name)) return;
		console.log('> Reading:', packId);

		const db = new nedb({ filename: path.join(packDest, pack.name), autoload: true });
		db.ensureIndex({ fieldName: '_id', unique: true });
		db.persistence.stopAutocompaction();

		const rdocs = await new Promise((resolve) => db.find({}, (err, rv) => resolve(rv)));
		console.log(' ? Entries:', rdocs.length);
		if (rdocs.length === 0) return void console.log('');

		console.log('Creating destination folder');
		const packSrcPath = path.join(packSourceBase, packId);
		fs.mkdirSync(packSrcPath, { recursive: true });

		console.log('Writing JSON exports:', packSrcPath);
		for (const d of rdocs) {
			const { _id, name } = d;
			const packFileDest = path.join(packSourceBase, packId, `${slugify(name)}.json`);
			fs.writeFileSync(packFileDest, JSON.stringify(d, null, '\t'));
			console.log(' +', packFileDest);
		}
	}

	for (const pack of packs)
		await unpackPack(pack);
}

const compactCompendiums = () => {
	const packs = [];
	console.log('Reading packed data');
	fs.readdirSync(packDest, { withFileTypes: true }).forEach((d) => {
		if (!d.isDirectory()) packs.push({ name: d.name, path: path.join(packDest, d.name) });
	});

	for (const pack of packs) {
		if (focus.length && !focus.includes(pack.name)) return;
		const db = new nedb({ filename: path.join(packDest, pack.name), autoload: true });
		db.ensureIndex({ fieldName: '_id', unique: true });
		db.persistence.stopAutocompaction();
		console.log('Compacting:', pack.name);
		db.persistence.compactDatafile();
	}
}

const emptyCompendiums = async () => {
	const packs = [];
	console.log('Reading packed data');
	fs.readdirSync(packDest, { withFileTypes: true }).forEach((d) => {
		if (!d.isDirectory()) packs.push({ name: d.name, path: path.join(packDest, d.name) });
	});

	for (const pack of packs) {
		if (focus.length && !focus.includes(pack.name)) return;
		const db = new nedb({ filename: path.join(packDest, pack.name), autoload: true });
		await new Promise(resolve => db.remove({}, { multi: true }, () => db.loadDatabase(() => resolve())));
	}
}

if (doUnpacking) await unpackCompendiums();
else if (doPacking) await packCompendiums();
else if (doCompact) await compactCompendiums();
else if (doEmpty) await emptyCompendiums();
