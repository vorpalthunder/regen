import { CFG } from './common.mjs';

/**
 * @returns {import('./regen-config-data.mjs').RegenConfigData}
 */
export function getSettings() {
	const cfg = game.settings.get(CFG.id, 'config');
	// Shim to simplify things and "override" the above datamodel
	cfg.chatcard = game.settings.get(CFG.id, 'chatcard');
	cfg.transparency = game.settings.get(CFG.id, 'transparency');
	cfg.autoBleed = game.settings.get(CFG.id, 'autoBleed');
	cfg.autoDefeat = game.settings.get(CFG.id, 'autoDefeat');
	cfg.autoStabilize = game.settings.get(CFG.id, 'autoStabilize');
	cfg.stablizeCard = game.settings.get(CFG.id, 'stabilizeCard');
	cfg.debug = game.settings.get(CFG.id, 'debug');

	try {
		cfg.ignoreRE = cfg.ignore.reduce((arr, re) => { arr.push(new RegExp(re, 'i')); return arr; }, []);
	}
	catch (err) {
		console.error('REGENERATION | Failure to compile regular expressions:\n', cfg.ignore);
		ui.notifications.error('Failure to compile regular expressions for regeneration.');
		cfg.ignoreRE = [];
	}

	return cfg;
}

/**
 * TODO: Treat positive and negative values separately.
 * @param {String} input
 * @param {Object} cfg
 */
export function getRegen(input, cfg) {
	input = input ?? '';
	if (input.length == 0) {
		if (cfg.debug) console.log('getRegen(n/a)');
		return 0;
	}
	if (cfg.debug) console.group(`getRegen("${input}")`);

	try {
		const vs = [0];
		for (let value of input.split(';')) {
			value = value.trim();
			const match = cfg.ignoreRE.find(f => f.test(value));
			if (match) {
				if (cfg.debug) console.warn(`Ignored: "${value}"`, 'Due:', match);
				continue;
			}

			if (/[([]/.test(value)) continue;
			const rv = value.match(/(?<value>-?\d+)/);
			const regenValue = rv?.groups.value;
			if (regenValue) vs.push(parseInt(regenValue, 10));
			else console.warn('Invalid:', value);
		}
		// Sort regen/fh parts and get the biggest
		const rg = Math.max(...vs);
		if (cfg.debug) {
			console.log('Numbers:', vs);
			console.log('Final:', rg);
		}
		return rg;
	}
	finally {
		if (cfg.debug) console.groupEnd();
	}
}
