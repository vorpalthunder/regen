import { RegenConfigData } from './regen-config-data.mjs';
import { CFG } from './common.mjs';
import { IgnorePatternConfig } from './ignore-pattern-config.mjs';

const migrateSettings = async () => {
	const s = game.settings.get(CFG.id, 'config');
	if (s.migrated == 0) {
		game.settings.set(CFG.id, 'chatcard', s.chatcard);
		game.settings.set(CFG.id, 'transparency', s.transparency);
		game.settings.set(CFG.id, 'autoBleed', s.autoBleed);
		game.settings.set(CFG.id, 'autoDefeat', s.autoDefeat);
		game.settings.set(CFG.id, 'autoStabilize', s.autoStabilize);
		game.settings.set(CFG.id, 'stabilizeCard', s.stabilizeCard);

		const ms = s.toObject();
		ms.migrated = 1;
		game.settings.set(CFG.id, 'config', ms);
		ui.notifications.info('Regeneration Configuration Migrated');
	}
	else {
		console.log('Regeneration Configuration already migrated');
	}
}

Hooks.once('init', () => {
	game.settings.register(CFG.id, 'config', {
		scope: 'world',
		type: RegenConfigData,
		config: false,
		default: new RegenConfigData(),
	});

	game.settings.register(CFG.id, 'chatcard', {
		name: 'Koboldworks.Regeneration.Config.ChatCardLabel',
		hint: 'Koboldworks.Regeneration.Config.ChatCardHint',
		type: Boolean,
		default: true,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, 'transparency', {
		name: 'Koboldworks.Regeneration.Config.TransparencyLabel',
		hint: 'Koboldworks.Regeneration.Config.TransparencyHint',
		type: Boolean,
		default: false,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, 'autoBleed', {
		name: 'Koboldworks.Regeneration.Config.AutoBleedLabel',
		hint: 'Koboldworks.Regeneration.Config.AutoBleedHint',
		type: Boolean,
		default: true,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, 'autoDefeat', {
		name: 'Koboldworks.Regeneration.Config.AutoDefeatLabel',
		hint: 'Koboldworks.Regeneration.Config.AutoDefeatHint',
		type: Boolean,
		default: false,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, 'autoStabilize', {
		name: 'Koboldworks.Regeneration.Config.AutoStabilizeLabel',
		hint: 'Koboldworks.Regeneration.Config.AutoStabilizeHint',
		type: Boolean,
		default: true,
		scope: 'world',
		config: true,
	});

	game.settings.register(CFG.id, 'stabilizeCard', {
		name: 'Koboldworks.Regeneration.Config.StabilizeCardLabel',
		hint: 'Koboldworks.Regeneration.Config.StabilizeCardHint',
		type: Boolean,
		default: true,
		scope: 'world',
		config: true,
	});

	game.settings.registerMenu(CFG.id, 'config', {
		id: 'mkah-pf1-regen-config',
		label: 'Koboldworks.Regeneration.Config.IgnorePatternButton',
		name: 'Koboldworks.Regeneration.Config.IgnorePatternTitle',
		hint: 'Koboldworks.Regeneration.Config.IgnorePatternHint',
		scope: 'world',
		icon: 'fas fa-tint',
		type: IgnorePatternConfig,
		config: true,
		restricted: true,
	});

	game.settings.register(CFG.id, 'debug', {
		name: 'Koboldworks.Regeneration.Config.DebugLabel',
		hint: 'Koboldworks.Regeneration.Config.DebugHint',
		type: Boolean,
		default: false,
		scope: 'world',
		config: true,
	});
});

Hooks.once('ready', () => {
	if (game.user.isGM)
		migrateSettings();
});
