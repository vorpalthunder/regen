import { getRegen } from './utility.mjs';

export class RegenSource {
	value = null;
	source = null;
	origin;

	parse(source, cfg, origin) {
		this.source = source;
		this.value = getRegen(source, cfg);
		this.origin = origin;
	}
}

export class HealthValue {
	value;
	nonlethal;
	temp;
	max;

	constructor({ value = 0, nonlethal = 0, temp = 0, max = 0 } = {}) {
		this.value = value ?? 0;
		this.nonlethal = nonlethal ?? 0;
		this.temp = temp ?? 0;
		this.max = max ?? 0;
	}

	get effective() {
		return this.value + this.temp;
	}

	get valid() {
		return Number.isSafeInteger(this.value) && Number.isSafeInteger(this.nonlethal) && Number.isSafeInteger(this.temp);
	}
}

export class HealthDelta extends HealthValue { }

export class HealthState extends HealthValue {
	bleeding;

	dead;
}

export class ConditionState {
	bleeding;
	staggered;
}

export class HealthChange {
	#volume = 0;

	get volume() {
		return this.#volume;
	}

	get isActive() {
		return this.#volume !== 0;
	}

	adjust(value) {
		this.#volume += value;
	}

	increase(value) {
		this.#volume = Math.max(this.#volume, value);
	}
}

export class HealthChangeInstance {
	formula;

	value;

	origin;
	#doc;

	/**
	 * @param {String} formula
	 * @param {String} origin
	 * @param {DocWrapper} doc
	 */
	constructor(formula, origin, doc) {
		if (typeof formula === 'string')
			formula = formula.trim();

		this.#doc = doc;

		Object.defineProperties(this, {
			formula: {
				value: `${formula}`,
				enumerable: true,
			},
			origin: {
				value: origin,
				enumerable: true,
			},
			value: {
				value: typeof formula === 'number' ? formula : undefined,
				writable: true,
			}
		});
	}

	get possiblyValid() {
		switch (typeof this.formula) {
			case 'number':
				return true;
			case 'string':
				return this.formula.length > 0;
			default:
				return false;
		}
	}

	async parse() {
		// safeTotal/safeRoll is not async, but it's treated as one for matching Foundry behaviour
		if (typeof this.formula === 'string') {
			/* global RollPF */
			this.roll = await Roll.create(this.formula, this.#doc?.rollData ?? {}).evaluate({ async: true });
			this.value = this.roll.total;
		}
		else
			this.value = this.formula;

		return this;
	}

	get isActive() {
		return Number.isFinite(this.value) && this.value !== 0;
	}
}

export class Stabilization {
	roll;
	dc;
	stabilized = true;

	dcRoll = {
		roll: null,
		json: null,
	};

	get success() {
		return this.roll.total >= this.dc;
	}

	get check() {
		return this.roll.total;
	}

	async stabilize(actor) {
		const cmData = await actor.rollAbilityTest('con', { skipDialog: true, chatMessage: false });
		this.roll = Roll.fromJSON(unescape(cmData.rolls[0]));
		this.stabilized = this.success;
	}

	constructor(dc) {
		this.dc = dc;
	}
}
