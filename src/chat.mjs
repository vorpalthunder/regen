import { CFG, getUsers } from './common.mjs';

/**
 * Combat and prettify chat messages.
 *
 * @param {ChatMessage} cm
 * @param {JQuery} jq
 */
export function compactMessage(cm, jq) {
	// const settings = getSettings();
	const flags = cm.flags?.[CFG.id];
	if (!flags) return;
	const html = jq[0];

	const wt = html.querySelector('.whisper-to');
	if (wt) wt.style.display = 'none'; // Remove whisper target

	const sc = html.closest('.chat-message');
	if (!sc) return;
	sc.classList.add('koboldworks');
	if (flags.regeneration) sc.classList.add('regeneration');
	if (flags.stabilization) sc.classList.add('stabilization');
	if (flags.bleedout) sc.classList.add('bleedout');

	// Metadata
	const mh = sc.querySelector('.message-header');
	if (mh) {
		const ts = mh.querySelector('.message-timestamp');
		mh.classList.remove('message-header');
		if (ts) ts.style.display = 'none'; // Hide
	}

	// Move contents
	const ms = html.querySelector(`.${CFG.id}`);
	const sender = mh.querySelector('.message-sender');
	if (sender) {
		sender.classList.remove('message-sender');
		sender.classList.add('regenerator');
	}

	// Shuffle
	const body = document.createElement('div');
	body.classList.add('prime-message');
	mh?.prepend(body);
	body.append(sender, ...ms.childNodes);

	html.style.borderColor = '';
}

export function generateBaseCard(d, cfg) {
	return {
		type: CONST.CHAT_MESSAGE_TYPES.OOC,
		rollMode: cfg.transparency ? 'roll' : 'gmroll',
		speaker: ChatMessage.getSpeaker({ token: d.token.document }),
		whisper: cfg.transparency ? [] : getUsers(d.actor, CONST.DOCUMENT_PERMISSION_LEVELS.OBSERVER),
		flags: { [CFG.id]: {} }
	};
}

// TODO: Allow overriding this somehow?
export async function generateCard(d, cfg) {
	d.card.displayName = d.token.name;
	const cardData = generateBaseCard(d, cfg);
	cardData.flags[CFG.id].regeneration = true;
	cardData.content = await renderTemplate(CFG.cardTemplate, d);
	return cardData;
}

export async function generateStabilizationCard(d, cfg) {
	d.stabilization.json = escape(JSON.stringify(d.stabilization.roll));
	/* global RollPF */
	d.stabilization.dcRoll.roll = RollPF.safeRoll(`10[Base] - ${-(d.stabilization.dc - 10)}[Health]`);
	d.stabilization.dcRoll.json = escape(JSON.stringify(d.stabilization.dcRoll.roll));
	const data = generateBaseCard(d, cfg);
	data.flags[CFG.id].stabilization = true;
	data.content = await renderTemplate(CFG.stabilizationTemplate, d);
	return data;
}

export async function generateBleedoutCard(d, cfg) {
	const message = game.i18n.localize('Koboldworks.Regeneration.BledOut');
	const data = generateBaseCard(d, cfg);
	data.flags[CFG.id].bleedout = true;
	data.content = '<div>' + message + '</div>';
}
