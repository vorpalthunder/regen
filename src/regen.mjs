import { CFG } from './common.mjs';
import * as chat from './chat.mjs';

import './item-config-ui.mjs';
import { onCombatUpdate } from './combat-update.mjs';
import './modules/_init.mjs';
import './settings.mjs';

// TODO: Staggered condition at effective 0 HP.
// TODO: Unstagger at effective >0 HP.

CFG.cardTemplate = `modules/${CFG.id}/template/chatcard.hbs`;
CFG.stabilizationTemplate = `modules/${CFG.id}/template/stabilization.hbs`;
Hooks.once('setup', async () => loadTemplates([CFG.cardTemplate, CFG.stabilizationTemplate]));

Hooks.on('renderChatMessage', chat.compactMessage);

Hooks.on('updateCombat', onCombatUpdate);
// Hooks.on('updateWorldTime', onTimePassing);
