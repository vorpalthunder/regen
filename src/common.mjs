export const CFG = {
	label: '+- REGEN&BLEED',
	id: 'mkah-pf1-regen',
	SETTINGS: {

	},
	FLAGS: {
		combatState: 'combatState',
		bleed: 'bleed',
		regen: 'regen',
		fastHeal: 'fastHeal',
		dying: 'dying',
	},
	cardTemplate: undefined,
	stabilizationTemplate: undefined,
};

import * as data from './regen-data.mjs';

export const signNum = (num) => num >= 0 ? `+${num}` : `${num}`; // add + to positive numbers

export const getUsers = (thing, permission) => Object
	.entries(thing.ownership)
	.filter(([uid, perm]) => perm >= permission)
	.map(([uid, perm]) => game.users.get(uid))
	.filter(u => !!u);

/**
 * @param {Actor} actor
 */
export const getActiveOwners = (actor) => getUsers(actor, CONST.DOCUMENT_OWNERSHIP_LEVELS.OWNER).filter(u => u.active && !u.isGM);

/**
 * Get one active owner.
 */
export function getOneOwner(actor) {
	const owners = getActiveOwners(actor)
		.sort((a, b) => a.role > b.role ? -1 : a.role == b.role ? 0 : 1);
	if (owners.length == 0) return null; // early fail
	if (owners.length == 1) return owners[0]; // early valid
	const highest = owners[0].role;
	const remaining = owners.filter(u => u.role == highest)
		.sort((a, b) => a.id.localeCompare(b.id, null));
	const first = remaining[0];
	return first;
}

export async function setDefeated(d, defeated) {
	try {
		await d.combat.updateEmbeddedDocuments('Combatant', [{ _id: d.combatant.id, defeated }]);
		const status = CONFIG.statusEffects.find(ae => ae.id === CONFIG.Combat.defeatedStatusId);
		const effect = d.token.actor && status ? status : CONFIG.controlIcons.defeated;
		await d.token.toggleEffect(effect, { active: true, overlay: true });
	}
	catch (err) {
		console.error('Error marking combatant defeated:', err);
	}
}

/**
 * @param {Actor} actor
 * @param {Combat} combat
 */
export function constructBaseData(actor, combat) {
	const actorData = actor.system;

	const d = {
		card: {
			css: CFG.id,
			// round: game.i18n.format('Koboldworks.Regeneration.Round', { round: combat.round }),
		},
		/** @type {Combat} */
		combat,
		/** @type {Combatant} */
		combatant: combat?.combatant,
		/** @type {Actor} */
		get actor() { return this.combatant?.actor; },
		/** @type {Token} */
		get token() { return this.combatant?.token?.object; }, // TokenPF
		fastheal: {
			value: new data.HealthChange,
			actor: new data.RegenSource,
			sources: [],
		},
		regen: {
			value: new data.HealthChange,
			actor: new data.RegenSource,
			sources: [],
		},
		bleed: {
			value: new data.HealthChange,
			sources: [],
		},
		/** @type {import('./doc-wrapper.mjs').DocWrapper[]} */
		items: [],
		itemFlags: {
			get any() {
				return this.bleeding || this.regen || this.dying || this.fastHeal;
			},
			/** @type {Boolean} */
			bleeding: actor.hasItemBooleanFlag(CFG.FLAGS.bleed),
			/** @type {Boolean} */
			regen: actor.hasItemBooleanFlag(CFG.FLAGS.regen),
			/** @type {Boolean} */
			fastHeal: actor.hasItemBooleanFlag(CFG.FLAGS.fastHeal),
			/** @type {Boolean} */
			dying: actor.hasItemBooleanFlag(CFG.FLAGS.dying), // Dying state and needs to stabilize
		},
		dead: false, // Dead in general
		diedNow: false, // Died on this pass
		past: new data.HealthState(actorData.attributes.hp),
		post: new data.HealthState(actorData.attributes.hp),
		delta: new data.HealthDelta(),
		conditions: new data.ConditionState(),
		stabilization: null,
	};

	return d;
}

/**
 * Tests if an item is relevant to this module's functionality.
 *
 * @param {ItemPF} item
 * @returns {Boolean}
 */
export const isRelevantItem = (item) => item.getItemBooleanFlags().some(f => [CFG.FLAGS.regen, CFG.FLAGS.bleed, CFG.FLAGS.fastHeal, CFG.FLAGS.dying].includes(f));

/**
 * Make sure number is actually a number.
 */
export const sanitizeNum = (num) => Number.isFinite(num) ? num : 0;
