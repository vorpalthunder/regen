import { CFG } from './common.mjs';
import { getSettings } from './utility.mjs';
import { RegenConfigData } from './regen-config-data.mjs';

export class IgnorePatternConfig extends FormApplication {
	regen = undefined;

	constructor(object, options) {
		super(object, options);

		this.regen = getSettings();
	}

	get template() {
		return `modules/${CFG.id}/template/ignore-pattern-config.hbs`;
	}

	static get defaultOptions() {
		const _default = super.defaultOptions;
		return {
			..._default,
			id: 'pf1-regen-config',
			title: game.i18n.localize('Koboldworks.Regeneration.Config.IgnorePatternTitle'),
			width: 420,
			height: 'auto',
			closeOnSubmit: false,
			submitOnChange: true,
			submitOnClose: false,
		};
	}

	getData(options) { // return can be an object or Promise
		const data = super.getData(options);
		data.regen = this.regen;
		if (data.regen.ignore.length === 0 || data.regen.ignore.at(-1)?.length > 0)
			data.regen.ignore.push('');
		return data;
	}

	_onResetDefaults(event) {
		event.preventDefault();

		this.regen = new RegenConfigData();
		ui.notifications.warn('Save changes to finalize reset.');

		this.render(false);
	}

	async _save(event) {
		event.preventDefault();
		await this.submit({ preventClose: true, preventRender: true }); // submit pending changes

		let errors = 0;
		const saveData = this.regen.toObject();
		saveData.ignore = saveData.ignore
			.map(f => f.trim()) // trim
			.filter(f => f.length); // remove empty

		// Basic RE testing
		saveData.ignore.forEach(il => {
			try {
				'test'.match(il);
			}
			catch (err) {
				console.log(CFG.label, '| Bad ignore pattern:', il, err);
				ui.notifications.error('Bad ignore pattern: ' + il, { permanent: true, console: false });
				errors++;
			}
		});

		if (errors <= 0) {
			game.settings.set(CFG.id, 'config', saveData);
			this.close();
		}
	}

	/**
	 * @param {Event} ev
	 * @param {Object} formData
	 */
	_updateObject(ev, formData) {
		formData = expandObject(formData);
		Object.entries(formData.ignore)
			.forEach(([idx, value]) => {
				if (value.length == 0) delete formData.ignore[idx];
			});
		this.regen = new RegenConfigData(formData);
	}

	/**
	 * @param {JQuery} jq
	 */
	activateListeners(jq) {
		super.activateListeners(jq);

		const [html] = jq;

		const app = this;

		html.querySelector('button[name="reset"]')
			.addEventListener('click', this._onResetDefaults.bind(this));

		html.querySelector('button[name="save"]')
			.addEventListener('click', this._save.bind(this));

		html.querySelector('.add-pattern')
			.addEventListener('click', async ev => {
				ev.preventDefault();
				await app.submit({ preventClose: true, preventRender: true }); // submit pending changes
				if (app.regen.ignore.length == 0 || app.regen.ignore.at(-1).length > 0) {
					app.regen.ignore.push('');
					app.render(false);
				}
				else
					ui.notifications.warn('Not adding more empty patterns.');
			});

		html.querySelectorAll('.remove-pattern').forEach(el => {
			el.addEventListener('click', async function(ev) {
				ev.preventDefault();
				await app.submit({ preventClose: true, preventRender: true }); // submit pending changes
				const idx = parseInt(this.dataset.index);
				app.regen.ignore.splice(idx, 1);
				app.render(false);
			});
		});
	}
}
