import { CFG } from './common.mjs';

let itemConfigTemplate;

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 * @param {Object} options
 */
function injectItemConfig(sheet, [html], options) {
	const item = sheet.item;

	// Ignore some item types
	if (['race', 'consumable', 'spell', 'class', 'container'].includes(item.type)) return;

	const ad = html.querySelector('.tab[data-tab="advanced"] div');
	if (!ad) return;

	const regenFormula = item.getItemDictionaryFlag(CFG.FLAGS.regen);
	const bleedFormula = item.getItemDictionaryFlag(CFG.FLAGS.bleed);
	const fastHealFormula = item.getItemDictionaryFlag(CFG.FLAGS.fastHeal);

	const templateData = {
		regenFormula,
		bleedFormula,
		fastHealFormula,
	};

	const div = document.createElement('div');
	div.innerHTML = itemConfigTemplate(templateData);

	async function updateValue(ev) {
		ev.preventDefault();
		ev.stopImmediatePropagation();

		const el = ev.target;

		const flag = CFG.FLAGS[el.dataset.target];
		if (!flag) return;

		const value = el.value.trim();
		if (value) {
			await item.setItemDictionaryFlag(flag, value);
			await item.addItemBooleanFlag(flag);
		}
		else {
			await item.removeItemDictionaryFlag(flag);
			if (item.system.level == null)
				await item.removeItemBooleanFlag(flag);
		}
	}

	div.querySelectorAll('input').forEach(el => {
		if (sheet.isEditable) {
			el.addEventListener('change', updateValue);
		}
		else {
			el.readOnly = true;
			el.disabled = true;
		}
	});

	ad.append(...div.childNodes);
}

Hooks.once('setup', () => {
	getTemplate(`modules/${CFG.id}/template/item-config.hbs`).then(t => itemConfigTemplate = t);
});

Hooks.on('renderItemSheet', injectItemConfig);
