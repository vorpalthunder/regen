/**
 * Wraps document for caching roll data.
 */
export class DocWrapper {
	/** @type {Actor|Item} */
	document;

	/** @type {Object} */
	#rollData;

	/**
	 * @returns {Object}
	 */
	get rollData() {
		this.#rollData ??= this.document.getRollData();
		return this.#rollData;
	}

	constructor(item) {
		this.document = item;
	}
}
