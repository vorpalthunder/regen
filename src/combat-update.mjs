import { CFG, getOneOwner, setDefeated, constructBaseData, isRelevantItem, sanitizeNum, signNum } from './common.mjs';
import { getSettings } from './utility.mjs';
import * as chat from './chat.mjs';
import * as data from './regen-data.mjs';
import { DocWrapper } from './doc-wrapper.mjs';

const printRegenLog = (actor, d, settings) => {
	const output = [
		`${CFG.label} | ${actor.name} [${actor.id}] changes:`,
		// Health
		`${signNum(d.delta.value)} HP ` +
		game.i18n.format('Koboldworks.Regeneration.HPChange', { old: d.past.value, new: d.post.value }),
		// Nonlethal
		`; ${signNum(-d.delta.nonlethal)} NL ` +
		game.i18n.format('Koboldworks.Regeneration.HPChange', { old: d.past.nonlethal, new: d.post.nonlethal }),
		// Temp
		`; ${signNum(-d.delta.temp)} THP ` +
		game.i18n.format('Koboldworks.Regeneration.HPChange', { old: d.past.temp, new: d.post.temp }),
	];

	const bled = d.past.bleeding !== d.conditions.bleeding;
	if (d.bleed.value.isActive) output.push(`; Bled ${d.bleed.value.volume}`);
	if (bled) output.push('; ' + game.i18n.localize('Koboldworks.Bleed.Stopped'));

	if (d.conditions.bleeding) output.push('; ' + game.i18n.localize('Koboldworks.Bleed.Ongoing'));
	if (d.dead) output.push('; ' + game.i18n.localize('Koboldworks.Regeneration.Defeated'));

	// Combine output and print to console
	console.log(output.filter(i => i.length).join(''), '\n', { data: d });
}

class SanityCheckError extends Error {}

export function sanityCheck(updateData) {
	const numbers = ['data.attributes.hp.value', 'data.attributes.hp.nonlethal', 'data.attributes.hp.temp'];
	const booleans = ['data.attributes.conditions.bleed'];

	const foundNonNumber = numbers.some(k => k in updateData && typeof updateData[k] !== 'number');
	const nB = booleans.some(k => k in updateData && typeof updateData[k] !== 'boolean');
	if (nB || foundNonNumber) {
		const et = new Error(); // for adjusting line number
		console.error('ERROR', nB, updateData[nB], foundNonNumber, updateData[foundNonNumber]);
		throw new SanityCheckError('REGENERATION: Internal Error: Sanity check failed', undefined, et.lineNumber - 1);
	}
}

/**
 * @param {Combat} combat
 * @param {Object} _round
 * @param {Object} _options
 * @param {String} _userId
 */
export async function onCombatUpdate(combat, round, _options, _userId) {
	if (!combat.started)
		return;

	const combatant = combat.combatant, actor = combatant.actor;
	if (!actor?.isOwner)
		return; // Combatant has no actor or current user is not its owner

	const defeated = combat.combatant?.isDefeated ?? true;
	if (defeated)
		return; // Don't process tokens that are defeated.

	if (actor.hasPlayerOwner) {
		// Process with player owner if they exist
		const owner = getOneOwner(actor);
		if (owner && owner.id !== game.user.id)
			return;
	}

	const settings = getSettings();
	if (settings.debug) {
		console.group(CFG.label);
		console.log('Combat round:', round, 'Turn:', combat.turn, 'ID:', combat.id);
		console.log('Settings:', settings);
	}

	// TODO: Make this happen on the side of the token's owner where possible instead of doing it only on GM side
	// ^ Debatable usefulness. GM is already present since combat was updated.
	const d = constructBaseData(actor, combat);

	// TODO: Cache this result somehow until actor is updated?
	d.items = d.itemFlags.any ? actor.items.filter(i => i.isActive && isRelevantItem(i)).map(i => new DocWrapper(i)) : [];

	const lastState = actor.getFlag(CFG.id, CFG.FLAGS.combatState);
	if (lastState?.id === combat.id && lastState?.round === combat.round)
		return console.warn(`${CFG.label} | Already applied to ${actor.name} this round.`);

	// Sanitize PF1 data
	d.past.value = sanitizeNum(d.past.value);
	d.past.temp = sanitizeNum(d.past.temp);
	d.past.nonlethal = sanitizeNum(d.past.nonlethal);

	const actorData = actor.system;

	const actorWrapper = new DocWrapper(actor);

	// --- Collect data ---
	try {
		if (settings.debug)
			console.log('Actor:', actor.name, actor.id, actor);
		d.past.bleeding = d.conditions.bleeding = d.itemFlags.bleeding;
		/*
		if (d.past.bleeding && settings.autoBleed)
			d.bleed.sources.push(new data.HealthChangeInstance('1', 'actor', actorWrapper));
		*/
		d.past.staggered = d.conditions.staggered = actorData.attributes.conditions.staggered;

		if (settings.debug)
			console.group('Regeneration');
		d.regen.actor.parse(actorData.traits.regen, settings, 'actor');
		d.regen.sources.push(new data.HealthChangeInstance(d.regen.actor.value, 'actor', actorWrapper));
		if (settings.debug)
			console.groupEnd();

		if (settings.debug)
			console.group('Fast healing');
		d.fastheal.actor.parse(actorData.traits.fastHealing, settings, 'actor');
		d.fastheal.sources.push(new data.HealthChangeInstance(d.fastheal.actor.value, 'actor', actorWrapper));
		if (settings.debug)
			console.groupEnd();

		if (settings.debug)
			console.group('Items');
		const healthChangePromises = [], pB = [], pFh = [], pR = [];
		for (const iw of d.items) {
			const item = iw.document;
			const itemData = item.system;
			let fastHeal, bleed, regen;
			if (item.hasItemBooleanFlag(CFG.FLAGS.regen)) {
				const raw_Regen = item.getItemDictionaryFlag(CFG.FLAGS.regen);
				if (raw_Regen)
					regen = new data.HealthChangeInstance(raw_Regen, item.id, iw);
				else
					regen = new data.HealthChangeInstance(`${itemData.level || 0}`, item.id, iw); // Fallback

				if (settings.debug)
					console.log('Regen:', regen.formula);

				if (regen.possiblyValid)
					healthChangePromises.push(regen.parse().then(r => {
						if (r.isActive)
							pR.push(r);
					}));
			}
			if (item.hasItemBooleanFlag(CFG.FLAGS.fastHeal)) {
				const raw_fastHeal = item.getItemDictionaryFlag(CFG.FLAGS.fastHeal);
				if (raw_fastHeal)
					fastHeal = new data.HealthChangeInstance(raw_fastHeal, item.id, iw);
				else
					fastHeal = new data.HealthChangeInstance(`${itemData.level || 0}`, item.id, iw); // Fallback

				if (settings.debug)
					console.log('FastHeal:', fastHeal.formula);

				if (fastHeal.possiblyValid)
					healthChangePromises.push(fastHeal.parse().then(fh => {
						if (fh.isActive)
							pFh.push(fh);
					}));
			}
			if (item.hasItemBooleanFlag(CFG.FLAGS.bleed)) {
				const raw_Bleed = item.getItemDictionaryFlag(CFG.FLAGS.bleed);
				if (raw_Bleed)
					bleed = new data.HealthChangeInstance(raw_Bleed, item.id, iw);
				else
					bleed = new data.HealthChangeInstance(`${itemData.level || 0}`, item.id, iw);

				if (settings.debug)
					console.log('Bleed:', bleed.formula);

				if (bleed.possiblyValid)
					healthChangePromises.push(bleed.parse().then(b => {
						if (b.isActive)
							pB.push(b);
					}));
			}
		}

		if (healthChangePromises.length) {
			await Promise.allSettled(healthChangePromises);
			if (pR.length) {
				if (settings.debug)
					console.group('Regeneration');
				try {
					pR.forEach(r => {
						if (settings.debug)
							console.log(r.formula, '=', r.value);
						d.regen.sources.push(r);
					});
				}
				finally {
					if (settings.debug)
						console.groupEnd();
				}
			}
			if (pFh.length) {
				if (settings.debug)
					console.group('Fast Healing');
				try {
					pFh.forEach(fh => {
						if (settings.debug)
							console.log(fh.formula, '=', fh.value);
						d.fastheal.sources.push(fh);
					});
				}
				finally {
					if (settings.debug)
						console.groupEnd();
				}
			}
			if (pB.length) {
				if (settings.debug)
					console.group('Bleeding');
				try {
					pB.forEach(b => {
						if (settings.debug)
							console.log(b.formula, '=', b.value);
						d.bleed.sources.push(b);
					});
				}
				finally {
					if (settings.debug)
						console.groupEnd();
				}
			}
		}

		if (settings.debug)
			console.groupEnd();
		if (settings.debug)
			console.log('Initial state:', deepClone(d));
	}
	finally {
		if (settings.debug)
			console.groupEnd();
	}

	// if (!token.actorLink) return; // don't handle unlinked tokens
	// TODO: Hook into options.advanceTime if it's ever used?
	// --- Process everything ---

	const msgs = [];

	if (settings.autoStabilize) {
		d.stabilization = new data.Stabilization(10 - d.post.value);

		// Dying
		if (d.itemFlags.dying) {
			d.stabilization.stabilized = false;

			// Stabilize
			if (settings.autoStabilize) {
				await d.stabilization.stabilize(actor);
				if (settings.debug)
					console.log('Stabilization:', d.stabilization);

				if (settings.stabilizeCard)
					msgs.push(await chat.generateStabilizationCard(d, settings));
			}
		}
	}

	// Apply bleeding damage
	if (settings.autoBleed && !d.stabilization?.stabilized)
		d.bleed.sources.push(new data.HealthChangeInstance(1, 'actor'));

	// Disable dying buff if successfully stabilized
	if (settings.autoStabilize && d.itemFlags.dying && d.stabilization.stabilized) {
		const dying = d.items.find(b => b.document.hasItemBooleanFlag(CFG.FLAGS.dying));
		if (dying) dying.document.setActive(false);
	}

	// --- Regeneration & Fast Healing ---
	// Configure
	function getBiggestHeal(list) {
		return list.reduce((total, current) => {
			if (current.isActive)
				total = Math.max(total, current.value);
			return total;
		}, -Infinity);
	}

	d.regen.value.increase(sanitizeNum(getBiggestHeal(d.regen.sources)));
	d.fastheal.value.increase(sanitizeNum(getBiggestHeal(d.fastheal.sources)));
	d.bleed.value.increase(sanitizeNum(getBiggestHeal(d.bleed.sources)));

	if (settings.debug) {
		console.group('Effective');
		if (d.regen.value.isActive)
			console.log('Regen:', d.regen.value);
		if (d.fastheal.value.isActive)
			console.log('Fast Heal:', d.fastheal.value);
		if (d.bleed.value.isActive)
			console.log('Bleed:', d.bleed.value);
		console.groupEnd();
	}

	// Apply
	// BLEED
	// Apply bleeding
	if (d.bleed.value.isActive) {
		let bleedRemaining = d.bleed.value.volume;
		// Bleed goes from temp HP first if available
		if (d.post.temp > 0) {
			d.post.temp -= bleedRemaining;
			bleedRemaining -= d.past.temp - d.post.temp;
			if (settings.debug)
				console.log('>>> Bleed from Temp HP:', d.past.temp - d.post.temp);
		}
		if (settings.debug)
			console.log('>>> Bleed from HP:', bleedRemaining);
		d.post.value -= bleedRemaining;
	}

	// REGEN & FASTHEAL
	let leftoverHealing = d.regen.value.volume; // ADD IN REGEN

	d.past.dead = d.past.effective <= -actorData.abilities.con.total;

	// Check if auto-defeat conditions apply
	// BUG: This works only on bleed of 1
	if (settings.autoDefeat) {
		const newCurHP = d.post.effective + leftoverHealing;
		d.post.dead = newCurHP <= -actorData.abilities.con.total;
		d.diedNow = d.post.dead && d.past.dead !== d.post.dead;
	}

	// Check if character died from bleed
	if (d.post.dead && !d.diedNow && d.regen.value.volume <= 0) {
		if (settings.debug)
			console.log(CFG.label, '|', actor.name, `[${actor.id}]`, 'was found dead.');
		return; // They're kinda dead
	}

	// Add in fast healing
	// Fast healing does not work when dead
	if (!d.post.dead)
		leftoverHealing += d.fastheal.value.volume;

	// Figure out if they have actually any fast healing or regeneration to do.
	if (leftoverHealing == 0 && !d.itemFlags.any && !d.stabilization?.roll) {
		if (settings.debug)
			console.log(CFG.label, '|', actor.name, `[${actor.id}]`, 'is a sad puppy and gets no cookie.');
	}
	else {
		const potentialHealing = leftoverHealing, updateData = {};

		// Apply regeneration
		// Nonlethal healing
		if (d.past.nonlethal > 0) {
			const v = d.post.nonlethal;
			d.post.nonlethal -= leftoverHealing;
			if (d.post.nonlethal < 0)
				d.post.nonlethal = 0;
			leftoverHealing -= d.past.nonlethal - v;
		}

		// Normal health healing
		if (d.post.value < d.past.max) {
			const v = d.post.value;
			d.post.value += leftoverHealing;
			if (d.post.value > d.post.max)
				d.post.value = d.post.max;
			leftoverHealing = d.post.value - v;
		}

		// --- Update Actor ---
		if (!d.post.valid) {
			console.error(CFG.label, '| SOMETHING WENT WRONG:', actor.name, d, { post: d.post });
			return;
		}

		// Update HP
		if (d.post.nonlethal !== d.past.nonlethal)
			updateData['data.attributes.hp.nonlethal'] = d.post.nonlethal;
		if (d.post.value !== d.past.value)
			updateData['data.attributes.hp.value'] = d.post.value;
		if (d.post.temp !== d.past.temp)
			updateData['data.attributes.hp.temp'] = d.post.temp;

		sanityCheck(updateData);

		d.delta.value = d.post.value - d.past.value;
		d.delta.nonlethal = -(d.past.nonlethal - d.post.nonlethal);
		d.delta.temp = d.post.temp - d.past.temp;

		// setFlag replacement to avoid effective double update
		updateData[`flags.${CFG.id}.${CFG.FLAGS.combatState}`] = { id: combat.id, round: combat.round };
		await actor.update(updateData);

		// --- Output ---
		if (settings.debug)
			console.log(CFG.label, '| DELTA:', d.delta);

		printRegenLog(actor, d, settings);

		if (d.delta.value !== 0 || d.delta.temp !== 0 || d.delta.nonlethal !== 0) {
			if (settings.debug) {
				console.group(CFG.label);
				console.log('Awaken:', d.past.effective, '<', d.past.nonlethal, '=', d.past.effective < d.past.nonlethal,
					'&', d.post.effective, '>=', d.post.nonlethal, '=', d.post.effective >= d.post.nonlethal);
				console.log('LEFTOVER', leftoverHealing, 'POTENTIAL', potentialHealing, 'ACTUAL', potentialHealing - leftoverHealing);
				console.log('Full Data:', d);
				console.groupEnd();
			}

			// Print a chatcard about the changes
			if (settings.chatcard)
				msgs.push(await chat.generateCard(d, settings));
		}
	}

	if (d.diedNow) {
		// Bled out chat card
		if (settings.bleedoutCard)
			msgs.push(await chat.generateBleedoutCard(d, settings));

		// Automatic defeat
		if (settings.autoDefeat && !d.combatant.defeated)
			await setDefeated(d, true);
	}

	if (msgs.length)
		await ChatMessage.create(msgs);
}
